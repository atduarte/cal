#include "Graphs.h"
#include <fstream>
#include <sstream>


/**

Funcao que pergunta ao utilizador a forma como quer realizar o input

@param Apontador para o grafo

*/
void getInput(Graph* city);


/**

Funcao que le a informacao pretendida manualmente

@param Apontador para o grafo

@return 0 success, !=0 not success
*/
int readInput(Graph* city);

/**

Funcao que le a informacao pretendida atraves de um ficheiro de texto

@param Nome do Ficheiro
@param Apontador para o grafo
@return 0 success, !=0 not success
*/
int filereader(string path,Graph* city);

