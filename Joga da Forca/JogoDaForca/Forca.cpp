#include "Forca.h"
#include "Util.h"

using namespace std;


void jogoClassico(string palavra, string tema)
{
	int nEspacos = 0;
	string palavraEscondida(palavra.length(), '_');

	for(int i = 0; i < palavra.size(); i++)
		if(palavra[i] == ' ') {
			palavraEscondida[i] = ' ';
			nEspacos++;
		}

	int tentativas = 6;

	bool flag_ganhou = false;

	while(true)
	{
		bool flag_acertou = false;
		system("cls");
		cout << "Modo de Jogo 2 - Classico" << endl 
			 << "Tema: " << tema << endl 
			 << "Tentativas restantes: " << tentativas << endl << endl;
		cout << "Palavra: " << palavraEscondida << "(" << palavraEscondida.size() - nEspacos << " letras)" << endl;

		drawMan(6-tentativas);

		string input;

		if(flag_ganhou) {
			cout << "Acertou. Parabens.";
			getline(cin, input);
			break;
		} else if(tentativas == 0) {
			cout << "Perdeu.";
			getline(cin, input);
			break;
		}		

		do {
			cout << "Digite uma letra: ";
			getline(cin, input);
			if(input.length() != 1)
				cout << "Tamanho invalido." << endl << endl;
		} while (input.length() != 1);


		for(unsigned int i = 0; i < palavra.length(); i++)
		{
			if(input[0] == palavra[i])
			{
				palavraEscondida[i] = palavra[i];
				flag_acertou = true;
			}
		}

		if(palavraEscondida == palavra)
			flag_ganhou = true;

		if(!flag_acertou)
			tentativas--;
	}
}

void jogoAvancado(string palavra, string tema)
{	
	int nEspacos = 0;

	string palavraEscondida(palavra.length(), '_');

	for(int i = 0; i < palavra.size(); i++)
		if(palavra[i] == ' ') {
			palavraEscondida[i] = ' ';
			nEspacos++;
		}

	int tentativas = 6;

	bool flag_ganhou = false;

	while(true)
	{
		bool flag_acertou = false;
		system("cls");
		cout << "Modo de Jogo 2 - Classico" << endl 
			 << "Tema: " << tema << endl 
			 << "Tentativas restantes: " << tentativas << endl << endl;
		cout << "Palavra: " << palavraEscondida << "(" << palavraEscondida.size() - nEspacos << " letras)" << endl;

		drawMan(6-tentativas);

		string input;

		if(flag_ganhou) {
			cout << "Acertou. Parabens.";
			getline(cin, input);
			break;
		} else if(tentativas == 0) {
			cout << "Perdeu.";
			getline(cin, input);
			break;
		}	

		cout << "Digite um conjunto de letras: ";

		getline(cin, input);

		vector<int> ocorrencias = KMPmacther(palavra, input);

		if(ocorrencias.size() > 0)
		{
			for(int i = 0; i < ocorrencias.size(); i++)
			{
				for(int j = 0; j < input.size(); j++)
				{
					palavraEscondida[ocorrencias[i]+j] = palavra[ocorrencias[i]+j];
					flag_acertou = true;
				}
			}
		}		

		if(palavraEscondida == palavra)
			flag_ganhou = true;
		else if(!flag_acertou)
			tentativas--;
	}

	if(flag_ganhou)
	{
		cout << "GANHOU " << endl;
		cout << "A PALAVRA ERA " << palavra << endl;
	}
	else
	{
		cout << "PERDEU " << endl;
		cout << "A PALAVRA ERA " << palavra << endl;
	}
}

void jogoAproximado(string palavra, string tema) {

	bool flag_ganhou = false;
	int nEspacos = 0;
	int tentativas = 6;
	string palavraEscondida(palavra.length(), '_');
	int d = 0;

	for(int i = 0; i < palavra.size(); i++)
		if(palavra[i] == ' ') {
			palavraEscondida[i] = ' ';
			nEspacos++;
		}

	while(true) {

		system("cls");
		cout << "Modo de Jogo 1 - Aproximado" << endl 
			 << "Tema: " << tema << endl 
			 << "Tentativas restantes: " << tentativas << endl << endl;
		cout << "Palavra: ";

		if(flag_ganhou)
			cout << palavra;
		else
			cout << palavraEscondida;
		
		cout << " (" << palavraEscondida.size() - nEspacos << " letras)" << endl;

		if(d != 0)
			cout << "Distancia foi de: " << d << endl;

		drawMan(6-tentativas);
		
		string input;

		if(flag_ganhou) {
			cout << "Acertou. Parabens.";
			getline(cin, input);
			break;
		} else if(tentativas == 0) {
			cout << "Perdeu.";
			getline(cin, input);
			break;
		}

		do {
			cout << "Digite uma palavra: ";
			getline(cin, input);
			if(input.length() != palavra.size())
				cout << "Tamanho invalido." << endl << endl;
		} while (input.length() != palavra.size());

		d = matchDistance(palavra, input);

		if(d == 0) {
			flag_ganhou = true;
		} else {
			tentativas--;
		}
		
	}
	
}